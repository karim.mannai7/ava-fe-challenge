# Avantgarde/Slash back end technical assessment

Karim Mannai Avantgarde/Slash back end technical assessment

## Installation

In your terminal run:
```bash
npm install
```

## Run the App

In your terminal run:
```bash
npm start
```

## Run tests

In your terminal run:
```bash
npm run test
```
The application will run by default on ports 3000 and 5173. there are 2 endpoints in the project:

1- Backend (nodeJs expressJs app): Running on port 3000

2- Frontend (Vue3 app): Running on port 5173
